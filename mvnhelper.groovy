
def runCommand(command) {
  def out = new StringBuilder()
  def err = new StringBuilder()
  def proc = command.execute()
  proc.waitForProcessOutput(out, err)
  [ proc.exitValue(), out.toString(), err.toString() ]
}

println 'Running maven...'
def res = runCommand([ 'mvn', '-X', 'clean', 'compile' ])
def stdout = res[1]
def stderr = res[2]

println 'Maven done.'

enum State {
  NONE,
  COMPILING,
  COMMAND
}

def state = State.NONE
for (line in stdout.readLines()) {
  if (line.startsWith('[ERROR]')) {
    println line
  }
  switch (state) {
    case State.NONE:
      if (line.startsWith('[DEBUG] Using compiler')) {
        state = State.COMPILING
      }
      break;
    case State.COMPILING:
      if (line.startsWith('[DEBUG] Command line options:')) {
        state = State.COMMAND
      }
      break;
    case State.COMMAND:
      println line.substring(8)
      state = State.NONE
      break;
  }
}
