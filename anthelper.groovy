
def runCommand(command) {
  def out = new StringBuilder()
  def err = new StringBuilder()
  def proc = command.execute()
  proc.waitForProcessOutput(out, err)
  [ proc.exitValue(), out.toString(), err.toString() ]
}

println 'Running ant...'
def res = runCommand([ 'ant', '-d', 'clean', 'build' ])
def stdout = res[1]
def stderr = res[2]

println 'Ant done.'

enum State {
  NONE,
  ARGS,
  FILES,
}

def prefix = '    [javac] '
def state = State.NONE
for (line in stdout.readLines()) {
  switch (state) {
    case State.NONE:
      if (line.startsWith(prefix + 'Compilation arguments:')) {
        state = State.ARGS
      }
      break;
    case State.ARGS:
      if (line.startsWith(prefix)) {
        if (line.startsWith(prefix + 'Files to be compiled:')) {
          state = State.FILES
        } else {
          def arg = line.substring(prefix.length())
          def matcher = arg =~ '\'(.*)\''
          if (matcher.matches()) {
            println matcher[0][1]
          }
        }
      } else {
        state = State.NONE
      }
      break;
    case State.FILES:
      if (line.startsWith(prefix + '    ')) {
        println line.substring(prefix.length() + 4)
      } else {
        state = State.NONE
      }
      break;
  }
}
